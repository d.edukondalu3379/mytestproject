import React from "react";
import logo from "./logo.svg";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img
          className="image"
          src="https://source.unsplash.com/random/400x200"
          alt="randomised!"
        />
      </header>
    </div>
  );
}

export default App;
